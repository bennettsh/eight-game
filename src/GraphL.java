import java.util.*;

/**
 * An adjacency list representation of a graph.
 * 
 * @author Scott Bennett
 */
public class GraphL {
    // An ArrayList of Integer LinkedLists
    private ArrayList<LinkedList> adjList;
    private int numVertices;
    private int numEdges;
    
    /**
     * Constructor.
     * Creates an empty graph (ie, no vertices).
     */
    public GraphL() {
        adjList = new ArrayList<LinkedList>();
        
        // initialize the ArrayList with one LinkedList
        // the graph will effectively be empty
        adjList.add(0, new LinkedList<Integer>() );
        adjList.trimToSize();
        
        numVertices = 0;
        numEdges = 0;
    }
    
    /**
     * Constructor.
     * Creates a graph with specified number of vertices.
     * 
     * @param numVertices
     */
    public GraphL(int numVertices) {
        adjList = new ArrayList<LinkedList>(numVertices);
        
        // set up all the LinkedLists
        for (int i = 0; i < numVertices; i++) {
            adjList.add(i, new LinkedList<Integer>() );
        }
        
        this.numVertices = numVertices;
        numEdges = 0;
    }
    
    /**
     * If this is a larger size, adds more vertices to the graph.
     * 
     * @param numVertices New number of vertices
     */
    public void resize(int numVertices) {
        if (numVertices > this.numVertices) {
            adjList.ensureCapacity(numVertices);
            
            // set up the new vertices, but don't add edges
            for (int i = this.numVertices; i < numVertices; i++) {
                adjList.add(i, new LinkedList<Integer>() );
            }
            
            adjList.trimToSize();
            
            this.numVertices = numVertices;
        }
        
    }
    
    /**
     * Returns the number of vertices in the graph.
     * 
     * @return Number of vertices
     */
    public int order() {
        return numVertices;
    }
    
    /**
     * Returns the number of edges in the graph.
     * 
     * @return Number of edges
     */
    public int size() {
        return numEdges;
    }
    
    /**
     * Returns an iterator to the LinkedList of neighbors
     * of vertex i.
     * 
     * @param i A vertex in the graph
     * @return An iterator of the neighbors of i
     */
    public Iterator<Integer> neighbors(int i) {
        // get the LinkedList at i and return it's
        // iterator starting at the first index
        return adjList.get(i).listIterator(0);
    }
    
    /**
     * Add an edge between two vertices, i and j.
     * 
     * @param i The first vertex
     * @param j The second vertex
     */
    public void addEdge(int i, int j) {
        // ensure i and j are actual vertices
        if (i < numVertices && j < numVertices) {
            adjList.get(i).add(j);
            adjList.get(j).add(i);
            numEdges++;
        }
    }

}
