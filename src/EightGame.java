import java.io.*;//FileReader;
import java.util.*;

/**
 * This Eight Game class reads in the file EightGame.dat to get a start state 
 * and proceeds to solve the puzzle. The final (goal) state is defined in the 
 * setUpGame method. Though this could be altered, my classes were designed 
 * with a specific final state in mind, so I am unsure if a different
 * final state would get solved correctly.
 * 
 * @author Scott Bennett
 */
public class EightGame extends AbstractGame {
    public static int NUM_SQUARES = 9; // number of squares on game board
    private EightGameState finalState; // the goal state  
    private Scanner input;
    
    GraphL moves;          // adjacency lists tell what squares are
                           // adjacent to a particular square 
    int tiles[];
    
    
    /**
     * Constructor
     * Sets up a hash table of size 16384 and makes a queue for a breadth
     * first search. 
     * Sets up an adjacency list for the squares on the board.
     */
    public EightGame() {
        super(16384, BFS);    // Use a breadth first search
        moves = new GraphL(10);
        tiles = new int[9];
        finalState = new EightGameState();
        input = new Scanner(System.in);
    }
    
    /**
     * Setup the game board and the starting state of the tiles.
     * The game board is always the same and is defined in the
     * EightGameState class.
     * 
     * Attempt to read the file EightGame.dat to get the start state.
     * EightGame.dat is laid out as follows:
     *    First digit is the position of the hole.
     *    Next 8 digits are the positions of each tile.
     * 
     * Change this file to change the start state.
     */
    public void setUpGame() {//String file) {
        EightGameState startState = makeInitialState();
        Scanner fileIn;
        int hole, t1, t2, t3, t4, t5, t6, t7, t8;
        
        // Make adjacency list of which squares are adjacent (no duplicates)
        moves.addEdge(0, 1);
        moves.addEdge(0, 3);
//        moves.addEdge(1, 0);        
        moves.addEdge(1, 2);
        moves.addEdge(1, 4);
//        moves.addEdge(2, 1);
        moves.addEdge(2, 5);
//        moves.addEdge(3, 0);
        moves.addEdge(3, 4);
        moves.addEdge(3, 6);
//        moves.addEdge(4, 1);
//        moves.addEdge(4, 3);
        moves.addEdge(4, 5);
        moves.addEdge(4, 7);
//        moves.addEdge(5, 2);
//        moves.addEdge(5, 4);
        moves.addEdge(5, 8);
//        moves.addEdge(6, 3);
        moves.addEdge(6, 7);
//        moves.addEdge(7, 4);
//        moves.addEdge(7, 6);
        moves.addEdge(7, 8);
//        moves.addEdge(8, 5);
//        moves.addEdge(8, 7);
        
        try {
            fileIn = new Scanner(new FileReader("EightGame.dat"));
            
            // Make the starting positions of the tiles
                  //startState.setState(1,0,2,3,4,5,6,7,8);   // Tester
            hole = fileIn.nextInt();
            t1 = fileIn.nextInt();
            t2 = fileIn.nextInt();
            t3 = fileIn.nextInt();
            t4 = fileIn.nextInt();
            t5 = fileIn.nextInt();
            t6 = fileIn.nextInt();
            t7 = fileIn.nextInt();
            t8 = fileIn.nextInt();
            
            startState.setState(hole, t1, t2, t3, t4, t5, t6, t7, t8);
        
            // Goal positions of the tiles
            finalState.setState(0, 1, 2, 3, 4, 5, 6, 7, 8);
        
            // Print a welcome screen
            System.out.println("\n\nWelcome to the Eight Game!\n");
            System.out.println("This simulator reads in a puzzle from EightGame.dat");
            System.out.println("and solves it before your very eyes!\n");
            System.out.println("Initial puzzle: ");
            //picture(startState);
            
            fileIn.close();
        } catch (IOException ioe) {
            System.out.println (ioe.getMessage());
            System.exit(1);
        }
        
        // Start the game
        startGame(startState);
    }
    
    /**
     * Determines if the current state is equal 
     * to the goal state.
     * 
     * @return True if current state is the goal state
     */
    @Override
    public boolean goalState() {
        return (currentState.equals(finalState));
    }
    
    /**
     * Make an initial EightGameState as determined by the
     * EightGameState's default constructor.
     * 
     * @return A default EightGameState
     */
    @Override
    public EightGameState makeInitialState() {
        return new EightGameState();
    }
    
    /**
     * Determines how the puzzle pieces can move based on
     * the current state.
     */
    @Override
    public void addChildren() {
        EightGameState newState = new EightGameState();  // where we hope to move to
        Iterator<Integer> itr;
        int numMoves = 8;  // Number of possible moves
        
        // locations of the current tile we want to move
        // and the position we want to move it to
        int currTile;
        int futurePos;
        
        for (int i = 1; i <= numMoves; i++) {  // Start with tile 1 (0 is hole)
            currTile = currentState.getState(i);
            
            newState.copy(currentState);  // copy current into nextState
            
            itr = moves.neighbors(currTile);  // get current's neighbors
            while (itr.hasNext()) {       // go through all the neighbors
                futurePos = itr.next();
                
                // test if the tile can shift left, right, etc...
                if (newState.shiftLeft(futurePos)) {
                    if (addNewState(newState)) {  // add the new state if shift was successful
                        newState = new EightGameState();
                    }
                } else
                if (newState.shiftRight(futurePos)) {
                    if (addNewState(newState)) {
                        newState = new EightGameState();
                    }
                } else
                if (newState.shiftUp(futurePos)) {
                    if (addNewState(newState)) {
                        newState = new EightGameState();
                    }
                } else 
                if (newState.shiftDown(futurePos)) {
                    if (addNewState(newState)) {
                        newState = new EightGameState();
                    }
                }
            }
        }
    }
    
    /**
     * Print a nice picture of the puzzle (board).
     * 
     * @param state A valid game state
     */
    @Override
    public void picture(EightGameState state) {
        int moveOn;
        
        for (int i = 0; i < 9; i++) {
            tiles[state.getState(i)] = i;
        }
        
        System.out.println(" _____ _____ _____");
        System.out.println("|     |     |     |");
        System.out.println("|  " + tiles[0] + "  |  " + tiles[1] + "  |  " + tiles[2] + "  |");
        System.out.println("|_____|_____|_____|");
        System.out.println("|     |     |     |");
        System.out.println("|  " + tiles[3] + "  |  " + tiles[4] + "  |  " + tiles[5] + "  |");
        System.out.println("|_____|_____|_____|");
        System.out.println("|     |     |     |");
        System.out.println("|  " + tiles[6] + "  |  " + tiles[7] + "  |  " + tiles[8] + "  |");
        System.out.println("|_____|_____|_____|");
        System.out.println();
        
        System.out.print( "****** press 0 and return to continue: ");
        
        moveOn = input.nextInt();
    }
    
    /**
     * Main method.
     * 
     * @param args 
     */
    public static void main(String[] args) {
        EightGame puzzle = new EightGame();
        /***********************************/
        puzzle.setUpGame();
        
        // Find a solution using a certain type of search algorithm
        if (puzzle.search()) {
            puzzle.printSolution();
            System.out.println("\nA solution was found!");
            
        } else {
            System.out.println("No solution found");
        }
        /*************************************/
        
        puzzle.printStats();
        
        /*******  Test Stuff  ************
        EightGameState t = new EightGameState();
        t.setState(8,7,6,5,4,3,2,1,0);
        puzzle.picture(t);
        t.shiftRight(1);
        puzzle.picture(t);
        t.shiftDown(4);
        puzzle.picture(t);
        t.shiftDown(7);
        System.out.println(t.getState(7));
        /**********************************/
    }
}
