
/**
 * Source: /u/css/classes/3460/Spr2013/search
 */
public interface State {
    @Override
    public int hashCode();
    @Override
    public boolean equals(Object anotherState);
    public void copy(State anotherState);
    public State clone();
}
