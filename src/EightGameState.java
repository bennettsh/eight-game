
/**
 * Represents a single state of the Eight Game puzzle.
 * 
 *    _____ _____ _____
 *   |     |     |     |
 *   |  0  |  1  |  2  |
 *   |_____|_____|_____|
 *   |     |     |     |
 *   |  3  |  4  |  5  |
 *   |_____|_____|_____|
 *   |     |     |     |
 *   |  6  |  7  |  8  |
 *   |_____|_____|_____|
 * 
 * 
 * A state is represented as an array: position.
 * There are eight real tiles, and one special tile (0) that is the 
 * "hole." The array subscripts correspond to the respective tiles.
 * The *value* of the cells in the array represents a tile's position.
 * 
 * @author Scott Bennett
 */
public class EightGameState implements State {
    private int position[];
    public static final int HOLE = 0;  // The 0 "tile" is the hole
    
    /**
     * Default constructor. Initialize tiles to default
     * start positions, which is also the goal state.
     */
    public EightGameState() {
        position = new int[9];
        setState(0,1,2,3,4,5,6,7,8);
    }
    
    /**
     * Constructor with the position of each tile.
     * 
     * @param t1 Tile 1
     * etc...
     */
    public EightGameState(int t0, int t1, int t2, int t3, int t4, int t5, int t6,
                          int t7, int t8) {
        position = new int[9];
        setState(t0,t1,t2,t3,t4,t5,t6,t7,t8);
    }
    
    /**
     * Allows you to set the input tiles to there positions.
     * 
     * @param t1 Tile 1
     * etc...
     */
    public void setState(int t0, int t1, int t2, int t3, int t4, int t5, int t6,
                         int t7, int t8) {
        position[t0] = 0;  // Set this tile to position 0
        position[t1] = 1;  // ... and so on
        position[t2] = 2;
        position[t3] = 3;
        position[t4] = 4;
        position[t5] = 5;
        position[t6] = 6;
        position[t7] = 7;
        position[t8] = 8;
    }
    
    /**
     * Returns the position of a given tile
     * 
     * @param tile
     * @return Position of tile
     */
    public int getState(int tile) {
        return position[tile];
    }
    
    /**
     * Attempt to shift a tile to the left.
     * 
     * @param tile
     * @return True if tile was shifted
     */
    public boolean shiftLeft(int tile) {
        if (position[tile] == 0 ||
            position[tile] == 3 ||
            position[tile] == 6) {
            return false;          // These are border positions
        }
        else if ((position[tile] - 1) == position[HOLE]) {  // Can shift left
            int temp = position[tile];
            position[tile] = position[HOLE];
            position[HOLE] = temp;
            return true;
        }
        
        return false;
    }
    
    /**
     * Attempt to shift a tile to the right.
     * 
     * @param tile
     * @return True if tile was shifted
     */
    public boolean shiftRight(int tile) {
        if (position[tile] == 2 ||
            position[tile] == 5 ||
            position[tile] == 8) {
            return false;          // These are border positions
        }
        else if ((position[tile] + 1) == position[HOLE]) {  // Can shift right
            int temp = position[tile];
            position[tile] = position[HOLE];
            position[HOLE] = temp;
            return true;
        }
        
        return false;
    }
    
    /**
     * Attempt to shift a tile up.
     * 
     * @param tile
     * @return True if tile was shifted
     */
    public boolean shiftUp(int tile) {
        if (position[tile] == 0 ||
            position[tile] == 1 ||
            position[tile] == 2) {
            return false;          // These are border positions
        }
        else if ((position[tile] - 3) == position[HOLE]) {  // Can shift up
            int temp = position[tile];
            position[tile] = position[HOLE];
            position[HOLE] = temp;
            return true;
        }
        
        return false;
    }
    
    /**
     * Attempt to shift a tile down.
     * 
     * @param tile
     * @return True if tile was shifted
     */
    public boolean shiftDown(int tile) {
        if (position[tile] == 6 ||
            position[tile] == 7 ||
            position[tile] == 8) {
            return false;          // These are border positions
        }
        else if ((position[tile] + 3) == position[HOLE]) {  // Can shift down
            int temp = position[tile];
            position[tile] = position[HOLE];
            position[HOLE] = temp;
            return true;
        }
        
        return false;
    }
    
    /**
     * Copies another EightGameState's values into this state.
     * 
     * @param other A different EightGameState
     */
    @Override
    public void copy(State other) {
        EightGameState comp = (EightGameState) other;
        
        for (int i = 0; i < position.length; i++) {
            position[i] = comp.position[i];
        }
    }
    
    /**
     * Returns a copy of this EightGameState.
     */
    @Override
    public EightGameState clone() {
        EightGameState newState = new EightGameState();
        newState.copy(this);
        return newState;
    }
    
    /**
     * Hashes the board to an integer.
     * 
     * @return The hash code
     */
    @Override
    public int hashCode() {
        return (position[0]*100000000 + position[1]*10000000 + position[2]*1000000 +
                position[3]*100000    + position[4]*10000    + position[5]*1000 +
                position[6]*100       + position[7]*10       + position[8]);
    }

    /**
     * Print the state's hash code.
     */
    public void printState() {
        System.out.println(hashCode());
    }
    
    /**
     * Checks to see if Object y is equal to this EightGameState.
     * 
     * @param y Object in question
     * @return True if y does equal this
     */
    @Override
    public boolean equals(Object y) {
        if (y instanceof EightGameState) {
            EightGameState x = (EightGameState) y;
            
            for (int i = 0; i < 2; i++) {
                if (position[i] != x.position[i]) {
                    return false;
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    /** Test stuff *************************
    public static void main(String[] args) {
        EightGameState test = new EightGameState();
        EightGameState test1 = new EightGameState();
        //System.out.println(test.position[0][0]);
        //System.out.println(test.position[2][2]);
        test.printState();
        //if (test.equals(test1)) System.out.println("True");
    }/**/
}
