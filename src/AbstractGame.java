import java.util.HashMap;
import java.util.Queue;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * Source: /u/css/classes/3460/Spr2013/search
 * 
 * This class was tailored to accommodate the Eight Game.
 */
public abstract class AbstractGame {//<EightGameState extends State> {
    public static final int BFS = 1;  // breadth first search
    public static final int DFS = 2;  // depth first search
    public static final int A1S = 3;  // best first search
    private int searchMethod;
    
    private HashMap<EightGameState, EightGameState> htable;
    protected EightGameState currentState; 
    private Queue<EightGameState> open;
    
    int childrenAdded;
    int children;
    int pathLength;

    public AbstractGame (int size, int searchType) {
       if (size > 0) {
          htable = new HashMap<EightGameState,EightGameState>(size);
          searchMethod = searchType;
          if (searchType == DFS)
              open = new LinkedList<EightGameState>();
          else if (searchType == BFS)
              open = new LinkedList<EightGameState>();
          else if (searchType == A1S)
              open = new PriorityQueue<EightGameState>(size,makeStateComparator());

          childrenAdded = 0;
          children = 0;
          pathLength = 0;
       }
    }

    public Comparator<EightGameState> makeStateComparator() {
        System.out.println ("To use best first search, ");
        System.out.println ("override this method in your subclass.");
        return null;  
    }

    public void startGame(EightGameState startState) {
       currentState = startState;
       startGame();
    }

    public void startGame() {
       if (currentState == null) {
           currentState = makeInitialState();
       }
       
       open.add(currentState);
       htable.put(currentState, null);
    }

    public void printSolution() {
       printInReverse(currentState);
       //picture(currentState);
    }

   private void printInReverse(EightGameState someState) {
      EightGameState newState;

      if (someState != null) {
          pathLength++;
          newState = htable.get(someState);
          printInReverse(newState);
          picture(someState);
      }
   } 

   public boolean search() {
       while(!open.isEmpty()) { 
           currentState = open.remove();
           
           if (goalState()) {
               return true;
           }
           
           addChildren();
       }
       
       return false;
    }

   @SuppressWarnings("unchecked")
   public boolean addNewState(EightGameState st) {
        children++;
        
        if (!htable.containsKey(st)) {
           htable.put(st, currentState);
           
           if (searchMethod == DFS)
               ((LinkedList)open).push(st);
           else
               open.add(st);
           
           childrenAdded++;
           return true;
        }
        
        return false;
   }

   public void printStats() {
        System.out.println ("\n\n");
        System.out.println ("Path contained " + pathLength + " steps.");
        System.out.println ("Children generated " + children);
        System.out.println ("Unique children generated " + childrenAdded);
   }

   abstract public void addChildren();
   abstract public void picture(EightGameState st);
   abstract public boolean goalState();
   abstract public EightGameState makeInitialState();
}