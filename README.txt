           The Eight Game
           --------------

This project was created in Netbeans.

      *** Requires Java 7 ***


The Eight Game is a tile puzzle with eight squares
and one hole. The puzzle is scrambled and the goal
is to put the tiles in order.

This program solves the eight game automatically. 

Initial puzzle state is provided in the form of a 
text file called EightGame.dat (example in the 
src folder). 


The EightGameStat class represents a single state 
of the Eight Game puzzle:

      _____ _____ _____
     |     |     |     |
     |  0  |  1  |  2  |
     |_____|_____|_____|
     |     |     |     |
     |  3  |  4  |  5  |
     |_____|_____|_____|
     |     |     |     |
     |  6  |  7  |  8  |
     |_____|_____|_____|
   
   
A state is represented as an array: position.
There are eight real tiles, and one special tile 
(0) that is the "hole." The array subscripts 
correspond to the respective tiles.The *value* of 
the cells in the array represents a tile's position.

The EightGame class reads in the file EightGame.dat
to get a start state and proceeds to solve the 
puzzle. The final (goal) state is defined in the 
"setUpGame" method. Though this could be altered, 
my classes were designed with a specific final state
in mind, so I am unsure if a different final state
would get solved correctly.